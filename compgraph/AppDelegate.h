//
//  AppDelegate.h
//  compgraph
//
//  Created by Александр Селиванов on 24.06.16.
//  Copyright © 2016 Alexander Selivanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

