//
//  MyOpenGLView.h
//  compgraph
//
//  Created by Александр Селиванов on 24.06.16.
//  Copyright © 2016 Alexander Selivanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MyOpenGLView : NSOpenGLView
{
    GLuint texture;
}

- (GLuint) loadTextureFromResource:(NSString *)name;
- (GLuint) loadTextureFromFile:(NSString *)fileName;

@end
