//
//  Vector3D.h
//  compgraph
//
//  Created by Александр Селиванов on 24.06.16.
//  Copyright © 2016 Alexander Selivanov. All rights reserved.
//

#ifndef Vector3D_h
#define Vector3D_h

class Vector3D {

public:
    float x;
    float y;
    float z;

public:
    Vector3D(float x, float y, float z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }
};

#endif /* Vector3D_h */
