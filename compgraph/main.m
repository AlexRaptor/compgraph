//
//  main.m
//  compgraph
//
//  Created by Александр Селиванов on 24.06.16.
//  Copyright © 2016 Alexander Selivanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
